#include<stdio.h>
int main()
{
    int  n, i, sum=0; float avg=0.00;
    printf("\n Enter how many numbers you want to enter : ");
    scanf("%d",&n);

    int arr[n];
    for(i=0; i<n; i++)
    {
        printf("\n Enter element %d : ",i+1);
        scanf("%d", &arr[i]);
    }

    for(i=0; i<n; i++)
    {
        sum=sum + arr[i];
        avg=(float)sum/(i+1);
    }

    printf("\n The avg of the %d elements of arrays is = %.2f\n", n, avg);

    return 0;
}