#include<stdio.h>
int input()
{
    int x;
    printf("\n Enter a number: ");
    scanf("%d", &x);
    return x;
}

int add(int x, int y)
{
    int sum=x+y;
    return sum;
}

void output(int x, int y, int sum)
{
    printf("\n The sum of %d and %d is = %d\n", x, y, sum);
}

int main()
{
    int a=input();
    int b=input();
    int sum=add(a,b);
    output(a,b,sum);
    return 0;
}